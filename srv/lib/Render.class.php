<?php

class Render {

	const PATH = 'srv/html';

	public static function load($page = null) {
		if (null !== $page) {
			$view = self::PATH . '/' . $page . '.html.php';
			if (is_readable($view)) {
				include $view;
			}
		}
	}

}