<!-- About Section -->
<section id="about-section" class="about-section section">
	<div class="container">
		<div class="section-title">
			<div class="section-title-more">
				first you should know
			</div>
			<div>
				<h2 class="section-title-heading">about <span>swiss</span></h2>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<p class="section-text wow text-right fadeInUp" data-wow-delay=".5s">Swiss Integrated Services is the distinguished player in the process of technology integration. Technology is changing the way we live our lives and run our business. It has been clearly seen that well designed and delivered IT solutions bring growth, profit and competitive advantage.</p>
			</div>
			<div class="col-sm-6 hidden-xs wow fadeInUp text-center">
				<div class="lg-gfx"><i class="fa fa-line-chart"></i></div>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-sm-6 hidden-xs wow fadeInUp margin-bottom-xs-40 text-center">
				<div class="lg-gfx"><i class="fa fa-briefcase"></i></div>
			</div>
			<div class="col-sm-6">
				<p class="section-text text-left wow fadeInUp" data-wow-delay="1s">Swiss Integrated Services develops major business in IT Consulting, Systems Integration and Value-added Reseller areas, running the very quick Information Technology evolution with leading and innovative offer.</p>
			</div>
		</div>

		<div class="row">
			<p class="section-text text-center wow fadeInUp margin-top-60" data-wow-delay="1.25s">Given its knowledge of specific solutions and due to Swiss Integrated Services engineers experience, the company addresses the main core issues of the various industrial sectors across European countries.</p>
		</div>

	</div>
	<!--/.container -->
</section>
<!--/ End About Section -->