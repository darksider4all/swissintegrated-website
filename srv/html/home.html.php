<!-- Home Section -->
<section id="home-section" class="home-section full-screen">
	<div class="cd-background-wrapper" data-top="transform: translate3d(0px, 0px, 0px)" data-top-bottom="transform: translate3d(0px, 300px, 0px)" data-anchor-target="#home-section">
		<div class="overlay"></div>
		<figure class="cd-floating-background">
			<img src="img/backgrounds/swissbg.jpg" alt="image-1">
		</figure>
	</div>
	<div class="hs-content">
		<div class="hs-content-inner">
			<div hidden>
			<div class=" init-animation-2 delay0-4s">
				<img src="img/logo-full-vertical.svg">
			</div>
			<div class="init-animation-2 delay0-6s">
				<h2 class="hs-text-6">aims to help your business run better by providing complex IT services, including IoT based solutions</h2>
			</div>
			</div>
		</div>
		<span class="scroll-down-arrow in-page-scroll" data-start="display: inline-block" data-100-start="display: none">
			<a href="#about-section" class="ci-icon-uniE930 init-animation-2 delay1-8s"></a>
		</span>
	</div>
</section>
<!--/ End Home Section -->