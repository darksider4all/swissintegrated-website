<!-- Job Section -->
<section style="" id="about-section" class="about-section section">
	<div class="container">
		<div class="section-title">
			<div class="section-title-more" style="color: #971514">
				now hiring
			</div>
			<div>
				<h2 class="section-title-heading">Cyber Security Engineer – Automotive </h2>
			</div>
		</div>
		<div  class="row" style="padding-bottom: 10vh;">
			<div class="col-sm-12">
				<h4><b>Job Description:</b></h4><br>
				<p class="section-text wow text-justify fadeInUp" data-wow-delay=".5s">
				Swiss Integrated Services is a distinguished player in the process of technology integration. Technology is changing the way we live our lives and run our business. It has been clearly seen that well designed and delivered IT solutions bring growth, profit and competitive advantage.
				</p>
				<p class="section-text text-justify wow fadeInUp" data-wow-delay=".5s">
				As Partner of a Fortune 100 company, Swiss Integrated Services is looking to extend its current services in the Automotive Sector targeting the quality of end customer vehicles, the environment, community and the world in which we live. We are seeking more experienced candidates for Cyber Security Engineer - Automotive positions in Bucharest, Petrosani or remote locations.
				</p>
				<p class="section-text text-justify wow fadeInUp" data-wow-delay=".5s">
				Vehicles are no longer simple mechanical devices but are controlled by dozens of digital computers coordinated by internal networks. While all this technology allowed better personalized experiences, seamless integration into a person’s lifestyle, differential services, predictive diagnostics and safer driving conditions, the increase in complexity drives the need for advanced security and data solutions. 
				</p>
			</div>
<!-- 			<div class="col-sm-6 wow fadeInUp text-center">
				<div class="lg-gfx"><i class="fa fa-line-chart"></i></div>
			</div> -->
		</div>
</div>
		<div class="clearfix"></div>
<div class="container">
		<div class="row">
			<div class="col-sm-6 wow fadeInUp margin-bottom-xs-40 text-center">
				<div class="lg-gfx"><i style="color: #971514 !important;" class="fa fa-check-square-o"></i></div>
			</div>
			<div class="col-sm-6">
				<ul id="list-title" class="section-text text-left wow fadeInUp" data-wow-delay="1s">
					<li><b>Job Requirements:</b></li>
					<li>Development and maintenance of automotive cyber security tools</li>
					<li>Developing new security requirements due to emerging threat technologies</li>
					<li>Threat analysis and risk assessment modeling</li>
					<li>Performing competitive analyses and maintain knowledge of emerging technologies in both the automotive and consumer electronics field</li>
					<li>Deal with huge Datasets on a regular basis</li>
					<li>Implement security controls, and develop procedures providing the foundations of the security posture</li>
					<li>Performing electronic communication systems testing to validate security</li>
				</ul>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-6 wow fadeInUp margin-bottom-xs-40 text-right">
				<p class="section-text text-center wow fadeInUp margin-top-60" data-wow-delay="1.25s">
				<ul id="list-title" class="section-text text-left wow fadeInUp" data-wow-delay="1.25s">
					<li><b>Basic Qualifications:</b></li>
					<li>Bachelor’s degree in Computer Science, Computer Engineering, Electrical Engineering or Mechanical Engineering</li>
					<li>2+ years’ experience in security, SOLID and OO design </li>
				</ul>
				</p>
			</div>
				<div class="col-sm-6 wow fadeInUp margin-bottom-xs-40 text-center margin-top-60">
				<div class="lg-gfx"><i style="color: #971514 !important;" class="fa fa-graduation-cap"></i></div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 wow fadeInUp margin-bottom-xs-40 text-center">
				<div class="lg-gfx"><i style="color: #971514 !important;" class="fa fa-check"></i></div>
			</div>
			<div class="col-sm-6 wow fadeInUp margin-bottom-xs-40 text-right">
				<p class="section-text text-center wow fadeInUp margin-top-60" data-wow-delay="1.25s">
				<ul id="list-title" class="section-text text-left wow fadeInUp" data-wow-delay="1.25s">
					<li><b>Preferred Qualifications:</b></li>
					<li>Knowledge of some of the following technologies: Unit Testing (NUnit sau MSTest), WCF, EF, ASP.NET MVC, WEBAPI, MSSQL, JavaScript, HTM5+CSS3, WPF, AngularJS</li>
					<li>General understanding of CAN network, Ethernet, firewalls and secure architecture development</li>
					<li>Knowledge with cryptographic algorithms and protocols</li>
					<li>Basic interest about IDA Pro, fuzz testers, network analyzer, threat modeling and risk management tools</li>
					<li>Experience with embedded hardware and software security knowledge</li>
					<li>Knowledge of digital and wireless communication and familiarity with communication technologies such as TCP/IP, Ethernet, Bluetooth, WiFi, DSRC and CAN</li>
					<li>Basic interest and knowledge about geographical technologies such as GPS, maps, geofencing and POIs as well as privacy risk associated with these technologies</li>
					<li>A genuine interest in technology, electronics, robotics, gadgets, security in the Internet of Things</li>
					<li>Strong problem-solving and conflict management skills</li>
				</ul>
				</p>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<p class="section-text wow text-justify fadeInUp" data-wow-delay=".5s">
				You’ll become part of a team that looks to the innovative world and it’s always ready to go a step further. Swiss Integrated Services is an equal opportunity employer committed to a culturally diverse workforce. All qualified applicants will receive consideration for employment without regard to race, religion, color, age, sex, national origin, sexual orientation and gender identity or disability status. 
				</p>
			</div>
<!-- 			<div class="col-sm-6 wow fadeInUp text-center">
				<div class="lg-gfx"><i class="fa fa-line-chart"></i></div>
			</div> -->
		</div>

	</div>
	<!--/.container -->
</section>
<!--/ End About Section