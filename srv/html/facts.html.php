<!-- Facts Section -->
<div id="facts-section" class="facts-section text-center font-second small-section">
	<div class="container">
		<div class="row">
			<div class="section-title">
				<div class="section-title-more">
					modern virtual tours
				</div>
				<div>
					<h2 class="section-title-heading">increase <span>sales</span></h2>
				</div>
			</div>
			<div class="col-sm-6 wow fadeInUp">
				<blockquote class="about-quote wow fadeInUp margin-bottom-40" data-wow-delay="0s">
					<p>"International buyers make up 20 to even 30% of home purchases."</p>
					<footer class="">National Association of Realtors&reg;</footer>
				</blockquote>
				<blockquote class="about-quote wow fadeInUp margin-bottom-xs-40" data-wow-delay=".5s">
					<p>A virtual reality company, which provides VR tours of college campuses, caused a “30% increase in physical visit requests” for participating schools, resulting in a 12.3% conversion rate.</p>
					<footer class="">Huffington Post</footer>
				</blockquote>
				
			</div>
			<div class="fact-item col-sm-3 col-xs-6 margin-bottom-60 margin-bottom-xs-60">
				<div class="fact-number font-second">
					<p class="counter">20</p><span>%</span>
				</div>
				<div class="fact-text">
					<p>international buyers</p>
				</div>
			</div>
			<!--/ End Fact Item -->
			<div class="fact-item col-sm-3 col-xs-6 margin-bottom-60 margin-bottom-xs-60">
				<div class="fact-number font-second">
					<p class="counter">12.5</p><span>%</span>
				</div>
				<div class="fact-text">
					<p>conversion rate</p>
				</div>
			</div>
			<!--/ End Fact Item -->
			<div class="fact-item col-sm-3 col-xs-6">
				<div class="fact-number font-second">
					<p class="counter">30</p><span>%</span>
				</div>
				<div class="fact-text">
					<p>physical visits</p>
				</div>
			</div>
			<!--/ End Fact Item -->                                                
			<div class="fact-item col-sm-3 col-xs-6">
				<div class="fact-number font-second">
					<p class="counter">100</p><span>%</span>
				</div>
				<div class="fact-text">
					<p>awesome</p>
				</div>
			</div>
			<!--/ End Fact Item -->
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container -->
</div>
<!--/ End Facts Section -->