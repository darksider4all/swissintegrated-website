<!-- Home Section -->
<section id="lemur-section" class="home-section full-screen">
	<div class="cd-background-wrapper" data-top="transform: translate3d(0px, 0px, 0px)" data-top-bottom="transform: translate3d(0px, 300px, 0px)" data-anchor-target="#lemur-section">
		<div class="lemur-overlay"></div>
		<figure class="cd-floating-background">
			<video width="auto" height="auto" autoplay="true" preload="true" loop="true">
  <source src="img/lemur/video1.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>
		</figure>
	</div>
	<div class="hs-content">
		<div class="hs-content-inner">
			<div hidden>
			<div class=" init-animation-2 delay0-4s">
				<svg version="1.1" id="lemur-svg" x="0px" y="0px" width="350" height="350" viewBox="0 0 512 512">
					<g fill="#fff">
						<g transform="matrix(1 0 0 1.102 529.2 -580.246)">
							<path d="M-360.7 726a27.5 27.5 0 0 0-27.5 27.4 27.5 27.5 0 0 0 27.5 27.5 27.5 27.5 0 0 0 27.5-27.6 27.5 27.5 0 0 0-27.5-27.5zm0 16.2a11.3 11.3 0 0 1 11.2 11.2 11.3 11.3 0 0 1-11.2 11.3 11.3 11.3 0 0 1-11.3-11.3 11.3 11.3 0 0 1 11.3-11.2zM-185.7 726a27.5 27.5 0 0 0-27.5 27.4 27.5 27.5 0 0 0 27.5 27.5 27.5 27.5 0 0 0 27.5-27.6 27.5 27.5 0 0 0-27.5-27.5zm0 16.2a11.3 11.3 0 0 1 11.2 11.2 11.3 11.3 0 0 1-11.2 11.3 11.3 11.3 0 0 1-11.3-11.3 11.3 11.3 0 0 1 11.3-11.2z"></path>
							<circle cx="-272.2" cy="820.1" r="11.1"></circle>
						</g>
						<path d="M44.2 61.7L117.8 164 74.6 317.6 256 450.3l181.4-132.7-43.3-154 73.8-102-88 46.3 9 36.5H123.2L132 108 44.3 61.6zm-18 5.2l-13.5 86.6 79.8 62 6.8-24.8L25 149.3 26 67zm459.7 0l1 82.3-74.3 41.5 6.8 25 79.8-62.2L486 67zM155 166l79.8 84.5-79.7 84.5c-50-50.5-50.8-117.8 0-169zm202 0c50.8 51.2 50 118.5 0 169L277 250.7l79.7-84.5zM256 275.8l76.7 87.8c-46 55.4-107 56-153.4 0l76.7-87.8z"></path>
					</g>
				</svg>
			</div>
			<div class="init-animation-2 delay0-6s">
				<h2 class="hs-text-6">Lemurcity </h2>
			</div>
			</div>
		</div>
		<span class="scroll-down-arrow in-page-scroll" data-start="display: inline-block" data-100-start="display: none">
			<a href="#about-section" class="ci-icon-uniE930 init-animation-2 delay1-8s"></a>
		</span>
	</div>
</section>
<!--/ End Home Section -->
<!-- About Section -->
<section id="about-section" class="about-section section" style="padding-top: 300px; padding-bottom: 0px !important">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12 col-xs-12">
				<div class="section-text wow text-right fadeInUp" data-wow-delay=".5s">
		<div id="gallery" style="display:none;">
			<img alt="Image 1 Title" src="img/lemur/1.jpg"
				data-image="img/lemur/1.jpg">
			<img alt="Image 2 Title" src="img/lemur/2.jpg"
				data-image="img/lemur/2.jpg">
			<img alt="Image 2 Title" src="img/lemur/3.jpg"
				data-image="img/lemur/3.jpg">
			<img alt="Image 2 Title" src="img/lemur/4.jpg"
				data-image="img/lemur/4.jpg">
			<img alt="Image 2 Title" src="img/lemur/5.1.jpg"
				data-image="img/lemur/5.1.jpg">
			<img alt="Image 2 Title" src="img/lemur/6.jpg"
				data-image="img/lemur/6.jpg">
			<img alt="Image 2 Title" src="img/lemur/8.jpg"
				data-image="img/lemur/8.jpg">
			<img alt="Image 2 Title" src="img/lemur/bucatarie1.jpg"
				data-image="img/lemur/bucatarie1.jpg">
			<img alt="Image 2 Title" src="img/lemur/bucatarie2.jpg"
				data-image="img/lemur/bucatarie2.jpg">
			<img alt="Image 2 Title" src="img/lemur/bucatarie3.jpg"
				data-image="img/lemur/bucatarie3.jpg">
			<img alt="Image 2 Title" src="img/lemur/casa_1.2.4.jpg"
				data-image="img/lemur/casa_1.2.4.jpg">
			<img alt="Image 2 Title" src="img/lemur/10.jpg"
				data-image="img/lemur/10.jpg">
			<img alt="Image 2 Title" src="img/lemur/11.jpg"
				data-image="img/lemur/11.jpg">
			<img alt="Image 2 Title" src="img/lemur/11.1.jpg"
				data-image="img/lemur/11.1.jpg">
			<img alt="Image 2 Title" src="img/lemur/12.jpg"
				data-image="img/lemur/12.jpg">
			<img alt="Image 2 Title" src="img/lemur/13.jpg"
				data-image="img/lemur/13.jpg">
			<img alt="Image 2 Title" src="img/lemur/14.jpg"
				data-image="img/lemur/14.jpg">	
			<img alt="Image 2 Title" src="img/lemur/15.jpg"
				data-image="img/lemur/15.jpg">	
			<img alt="Image 2 Title" src="img/lemur/16.jpg"
				data-image="img/lemur/16.jpg">				
		</div>
	</div>
		</div>

		<div class="clearfix"></div>
	</div>
	<!--/.container -->
</section>
		<script type="text/javascript"> 
			
			jQuery(document).ready(function(){ 
			jQuery("#gallery").unitegallery({
				tiles_type:"nested"
			});
			}); 
		
		</script>
<!--/ End About Section
