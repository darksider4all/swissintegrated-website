<!-- Footer Section -->
			<footer id="footer-section" class="footer-section section no-padding-bottom">
				<div class="social wrapper text-center init-animation-1">
					<!-- Social buttons -->
					<ul class="social-list cl-effect-5">
<!-- 						<li id="#facebook">
							<a href="" target="_blank">
								<span><i class="fa fa-facebook fa-3x"></i></span>	
							</a>
						</li> -->
						<li id="#twitter">
							<a href="https://twitter.com/SwissIntegrated" target="_blank" class="ripple-group">
								<span><i class="fa fa-twitter fa-3x"></i></span>	
							</a>
						</li>
						<li id="#linkedin">
							<a href="https://www.linkedin.com/company-beta/10864062/" target="_blank">
								<span><i class="fa fa-linkedin fa-3x"></i></span>	
							</a>
						</li>
						<li id="#youtube">
							<a href="https://www.youtube.com/channel/UC-QuetbH76mjmuN6UQIGA-w" target="_blank">
								<span><i class="fa fa-youtube fa-3x"></i></span>	
							</a>
						</li>
					</ul>
				</div>
				<!-- go top pop-up -->
				<div id="go-top" class="go-top go-top-out in-page-scroll">
					<div class="circle1"></div>
					<a href="#top" class="square"><span></span></a>
					<div class="rectangle"></div>
					<div class="circle2"></div>
				</div>
				<!--/ End go top pop-up -->
				<div class="footer-bottom">
					<!-- Copyright -->
					<span class="footer-bottom-text font-second" href="" target="_blank">Swiss Integrated Services © <?=date('Y')?></span>
					<!-- End Copyright -->
				</div>
			</footer>
			<!--/ End Footer Section -->