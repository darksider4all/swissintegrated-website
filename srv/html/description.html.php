<!-- About Section -->
<section id="about-section" class="about-section section" style="padding-top: 0px !important;">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<p class="section-text wow text-justify fadeInUp" data-wow-delay=".5s"><a href="http://lemurcity.com">Lemurcity</a> helps customers find their dream homes while also allowing real estate companies better showcase and sell houses through the help of modern technology by turning any house into a VR tour experience and thus providing customers with a better feel of the house. Furthermore, customers can ask for a house design and experiment in a safe 3D space environment any changes they want to bring to the house.</p>
			</div>
		</div>

		<div class="clearfix"></div>

	</div>
	<!--/.container -->
</section>
<!--/ End About Section -->