<!-- Process Section -->
<section id="process-section" class="process-section section">
	<div class="container">

		<div class="section-title">
			<div class="section-title-more">
				take a look at some of
			</div>
			<div>
				<h1 class="section-title-heading">our <span>services</span></h1>
			</div>
		</div>
		<div id="swiss-services" class="row benefits">
			<div class="col-xs-12 col-md-4 col-sm-12 card wow fadeInUp  data-wow-fadeInUp delay=".5s">
				<h1>IT Infrastructure</h1>
				<p>Gain scalability and flexibility with no effort/worries. We’ll help you find the most reliable infrastructure that covers all of your business's demands.
				</p><p>We provide multiple choices, from desktop virtualization, datacenter consolidation, green datacenter, environment-on-demand, remote infrastructure management,
				to global delivery models.</p>
			</div>
			<div class="col-xs-12 col-md-4 col-sm-12 card wow fadeInUp c-large" data-wow-fadeInUp delay="1s">
				<h1>Business Process</h1>
				<p>Get the help you need from our certified specialists in business and organizational management to make sure you're on track.</p><p>We provide business consulting
				services along with business process solutions such as: management processes, operational processes and supporting processes.</p><p>Our solutions cover everything
				from simple task-based processes to the knowledge-intensive, industry-specific processes.</p>
			</div>
			<div class="col-xs-12 col-md-4 col-sm-12 card wow fadeInUp c-large" data-wow-fadeInUp delay="1.5s">
				<h1>Application Development</h1>
				<p>Have an idea but don't possess the know-how of making it possible? Let us take care of those details. We provide web application development and mobile
					application development services.</p><p>We possess the know-how for the following technologies but learn something new everyday: WPF, WCF, WebAPI, EF, Microsoft
					Prism, SilverLight, WindowsPhone, AngularJS, ASP NET MVC, C#, PHP, PDO MySQL, DQL (Doctrine), HTML5, CSS3, SASS, JS, JQuery, AJAX.</p>
			</div>
			<div class="col-xs-12 col-md-4 col-sm-12 card wow fadeInUp  data-wow-fadeInUp delay="2s">
				<h1>Marketing Campaign</h1>
				<p>From business plans to product presentations, from advertising to corporate social networking, choose one of our services today and get your product
				or business out there.</p><p>We can write content for speeches, presentations and website along with website and presentations.</p>
			</div>
			<div class="col-xs-12 col-md-4 col-sm-12 card wow fadeInUp  data-wow-fadeInUp delay="2.5s">
				<h1>Innovation Research</h1>
				<p>We love new technologies and we love to get down to work. Our experts cover various fields and are on the constant lookout for improvement. If you're
				thinking about it, chances are we already did too.</p><p>Get in touch today to see how we can help you. We are good at Designing microcontrollers (AVR, Cortex)
				based systems and Embedded - C programming.</p>
			</div>
			<div class="col-xs-12 col-md-4 col-sm-12 card wow fadeInUp c-small" data-wow-fadeInUp delay="3s">
				<h1>Application Maintenance</h1>
				<p>We provide various solutions such as: defective correction, performance improvements and enhancements as well as scheduled maintenance.</p>
			</div>
			<div class="col-xs-12 col-md-4 col-sm-12 card wow fadeInUp c-small" data-wow-fadeInUp delay="3.5s">
				<h1>Survey</h1>
				<p style="margin-bottom: 25px;">Get to know the feedback from your clients and survey available markets and trends with the help of our services.</p>
			</div>
			<div class="col-xs-12 col-md-4 col-sm-12 card wow fadeInUp c-small" data-wow-fadeInUp delay="3.5s">
				<h1>CAD & 3D Rendering</h1>
				<p>Whether you are an architect, game designer, artist, manufacturer, engineer or simply passionate about this subject, we can heko you design ideas, visualize concepts through photorealistic renderings, and simulate how a design will perform in the real world.</p>
			</div>
			<div class="col-xs-12 col-md-6 col-sm-12 card wow fadeInUp c-small" data-wow-fadeInUp delay="3.5s">
				<h1>3D Printing</h1>
				<p>Want something 3D printed? Just send us a STL file, photograph or scan of the object you want printed, decided on the material, color and size and let us take care of the rest.</p>
			</div>
			<div class="col-xs-12 col-md-6 col-sm-12 card wow fadeInUp c-small" data-wow-fadeInUp delay="3s">
				<h1>PCB Optimization and Esim integration</h1>
				<p style="margin-bottom: 12px;">Stay up to date with the emergent technologies in the field of PCB Design and integrate new technologies such as eSim in your product’s design for better performance and securing a competitive advantage.</p>
			</div>
		</div>
	</div>
	<!-- /.container -->
</section>
<!--/ End Process Section -->
