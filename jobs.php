<?php
require_once 'srv/bootstrap.php';
?>

<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<title>Swiss Integrated Services</title>
	<!-- Bootstrap -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<!-- Animation Libraries -->
	<link rel="stylesheet" href="css/animate.min.css">
	<link rel="stylesheet" href="css/magic.min.css">
	<!-- Google Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900%7COpen+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
	<!-- Fonts Icons -->
	<link rel="stylesheet" href="css/custom-icons.css">
	<link rel="stylesheet" href="css/et-line-icons.css">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<!-- Nivo Lightbox -->
	<link rel="stylesheet" href="css/nivo-lightbox/nivo-lightbox.css">
	<link rel="stylesheet" href="css/nivo-lightbox/lightbox-theme.css">
	<!-- Animated Headlines -->
	<link rel="stylesheet" href="css/animated-hedlines.css">
	<!-- Youtube Background Player -->
	<!-- <link rel="stylesheet" href="css/YTPlayer.css"> -->
	<!-- Main Styles -->
	<link href="css/styles.css" rel="stylesheet">
	<!-- Theme -->
	<link href="css/theme/light-green.css" rel="stylesheet" data-color="" id="theme">
	<!-- Animsition -->
	<link href="css/animsition.min.css" rel="stylesheet">
	<!-- Favicon (retina) -->
	<link rel="icon" type="image/png" href="favicon.png">
	<!--[if IE]><link rel="shortcut icon" href="favicon.ico"><![endif]-->
</head>

<body>
	<!-- Page Wrapper -->
	<!--<div id="page" class="animsition equal" data-loader-type="loader3" data-page-loader-text="Swiss Integrated Services" data-animsition-in="zoom-outy" data-animsition-out="fade-out-up-sm" style="transform-origin: 50% 50vh;">-->
	<div id="page" style="transform-origin: 50% 50vh;">
		<div id="top"></div>
		<?=Render::load('job')?>
		<?=Render::load('navbar-second')?>

		<div id="page-2">
			<?=Render::load('contact')?>

			<?=Render::load('footer')?>
		</div>
		<!--/ #page-2 -->

	</div>
	<!--/ #page -->
	<!-- JS Scripts -->
	<script src="js/modernizr.min.js"></script>
	<script src="js/jquery-2.1.1.min.js"></script>
	<script src="js/jquery.easing.1.3.min.js"></script>
	<script src="js/jquery.animsition.min.js"></script>
	<script src="js/smoothscroll.js"></script>
	<script src="js/skrollr.min.js"></script>
	<script src="js/wow.min.js"></script>
	<script src="js/waypoints.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.counterup.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/animateSlider.js"></script>
	<script src="js/animated-headlines.js"></script>
	<script src="js/contactForm.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCnjFAPITzPVBDC735Nn8C2HvhmTMnBEcc"></script>
	<script src="js/scripts.js"></script>
	<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
	<!--/ End JS Scripts -->
</body>

</html>
